# GEOGUESSR RESULTS

## Description

> This tool is not affiliated and / or offered by Geoguessr

This webapp helps Geoguessr players to track their results by fetching challenges results and organizing them by 
week / scores.

![screenshot of the webapp displaying results table](img/readme_screenshot.png)

## Deploy

This is a simple serverless app so you can deploy it on any web hosting service.
I use AWS S3 for its simplicity.

Add your games in /js/games.js to display them.

## Update / add games

Update the /js/games.js file and upload it to your web hosting service.

## TODO

- Terraform to deploy on S3
- CICD
- get a friend who knows js to clean code :)
