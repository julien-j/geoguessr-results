const URL = "https://www.geoguessr.com/api/v3/results/scores/"

PLAYERS = []

function getData(game) {
    gameUrl = `${URL}${game.id}/0/100`
    let promise = new Promise((resolve, reject) => {
        return fetch(gameUrl).then(response => {
            if(response.ok) {
                resolve(response.json());
            } else {
                reject(new Error('error fetching data'));
            }
        }, error => {
            reject(new Error(error.message));
        })
    });  
    return promise;
}

function enrichData(game) {

    let promise = new Promise((resolve, reject) => {
        return getData(game).then(response => {
            game['results'] = response;
            resolve(game);
        }, error => {
            reject(new Error(error.message));
        })
    });
    return promise;
}

function getScores(games){
    let promises = []
    games.forEach(game => {
        promises.push(enrichData(game));
    })

    Promise.all(promises).then(games => {
        GAMES = games;
        createButtons(games);
        displayTop(games, setPlayersScores(games), 'all');
        updateWeek(games, Math.ceil(games.length / 7));
        displayScoresAllTime(games);
    });
}

function setPlayersScores(gamesSelected) {
    players = [];
    gamesSelected.forEach(game => {
        game.results.forEach(result => {
            if (!players.some(e => e.name === result.playerName)) {
                let new_player = {};
                new_player['name'] = result.playerName;
                new_player['totalScore'] = 0;
                new_player['results'] = [];
                players.push(new_player);
            }
            let gameResult = {};
            gameResult['date'] = game.date;
            gameResult['score'] = result.totalScore;
            
            players.forEach(player => {
                if(player.name === result.playerName){
                    player['results'].push(gameResult)
                    player['totalScore'] += result.totalScore
                }
            })
        })
    })
    return players.sort((a,b) => b.totalScore - a.totalScore);
}

document.addEventListener("DOMContentLoaded",function(){
    getScores(GAMES)
});