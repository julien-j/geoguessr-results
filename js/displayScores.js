function displayScoresAllTime(games) {  
    let players = setPlayersScores(games);

    console.log(players)

    players.forEach(player => {
        let averageScore = Math.round(player.totalScore / player.results.length);

        let playerData = [];
        playerData.push(`<td>${player.name}</td>`);
        playerData.push(`<td>${player.results.length}</td>`);
        playerData.push(`<td>${averageScore.toLocaleString('fr-FR')}</td>`);
        playerData.push(`<td>${player.totalScore.toLocaleString('fr-FR')}</td>`);

        createTableRow(playerData, 'all-results-body')
    })
}


function displayScores(games, players) {
    // table header
    let dataFirstRow = []
    dataFirstRow.push('<th scope="col">Player</th>');

    games.forEach(game => {
        dataFirstRow.push(`<th scope="col"><a href="https://www.geoguessr.com/challenge/${game.id}" target="_blank">${game.map}<br>${game.date}</a></th>`);
    })
    dataFirstRow.push('<th scope="col">Total</th>');
    createTableRow(dataFirstRow, 'results-header');

    // player row
    players.forEach(player => {
        let playerData = [];
        playerData.push(`<td>${player.name}</td>`);

        games.forEach(game => {
            let gameInPlayerResults = player.results.find(result => result.date === game.date);
            if(gameInPlayerResults){

                playerData.push(`<td>${gameInPlayerResults.score.toLocaleString('fr-FR')}</td>`);
            } else{
                playerData.push(`<td>-</td>`);
            }
        })
        playerData.push(`<td>${player.totalScore.toLocaleString('fr-FR')}</td>`);
        createTableRow(playerData, 'results-body')
    })
}

function updateWeek(games, weekNumber){

    // remove previous data
    removeChilds(document.querySelector('#results-header'));
    removeChilds(document.querySelector('#results-body'));

    let weekStart = (weekNumber - 1) * 7;
    let weekEnd = weekStart + 7;

    if(weekEnd > games.length) {
        weekEnd = games.length;
    }

    let gamesSelected = games.slice(weekStart, weekEnd);
    let players = setPlayersScores(gamesSelected);

    displayScores(gamesSelected, players);
    displayTop(gamesSelected, players, 'week');
    createPlayButtons(gamesSelected);
}
