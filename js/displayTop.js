function displayTop(games, players, type){
    let bestPlayer = "No data";
    let bestPlayerScore = 0;
    let singleGameHighscorePlayer = "No data";
    let singleGameHighscore = 0;
    let singleGameHighscoreMap = "No data";

    players.forEach(player => {
        if(player.totalScore > bestPlayerScore){
            bestPlayerScore = player.totalScore;
            bestPlayer = player.name;
        }
    })

    games.forEach(game => {
        game.results.forEach(result => {
            if(result.totalScore >= singleGameHighscore){
                singleGameHighscore = result.totalScore;
                singleGameHighscorePlayer = result.playerName;
                singleGameHighscoreMap = game.map;
            }
        })
    });

    if(type === 'all'){
        document.querySelector('#top-totalGames').innerHTML = games.length;
        document.querySelector('#top-bestName').innerHTML = bestPlayer;
        document.querySelector('#top-bestScore').innerHTML = `${bestPlayerScore.toLocaleString('fr-FR')} pts`;
    } else{
        document.querySelector('#top-weekBestName').innerHTML = singleGameHighscorePlayer;
        document.querySelector('#top-weekBestScore').innerHTML = `${singleGameHighscore.toLocaleString('fr-FR')} pts`;
        document.querySelector('#top-weekBestMap').innerHTML = singleGameHighscoreMap;
    }
}
