///////////////////////////
// WEEK SLECTION BUTTONS //
///////////////////////////

function createButtons(games) {
    let nb_buttons = Math.ceil(games.length / 7);
    let weekSelectors = document.querySelector('#week-selectors');

    for(let i = 1; i <= nb_buttons; i++){
        let button = document.createElement('button');
        button.classList.add('btn', 'btn-outline-primary');
        button.setAttribute('id', `button-${i}`);
        button.setAttribute('type', 'button');
        button.setAttribute('data-toggle', 'button');
        if(i === nb_buttons){
            button.setAttribute('aria-pressed', 'true');
            button.classList.add('active')
        } else{
            button.setAttribute('aria-pressed', 'false');
        }
        button.innerHTML = `Week ${i}`;
        weekSelectors.append(button);
    }
    setEventListener();
}

function setEventListener(){
    let buttons = document.querySelectorAll('#week-selectors > .btn')
    buttons.forEach(button => {
        button.addEventListener('click', function(event){
            buttons.forEach(reButton => {
                if(reButton !== this) {
                    reButton.classList.remove('active');
                }
            })
            updateWeek(GAMES, event.target.id.split("-")[1])
        })
    })
}

//////////////////
// PLAY BUTTONS //
//////////////////

function createPlayButtons(games){
    // remove remaining elements
    removeChilds(document.querySelector('#playButtonsGroup'));


    if(games.length > 0) {
        let buttonsGroup = document.querySelector('#playButtonsGroup');

        let playPreviousGames = document.createElement('div');
        playPreviousGames.classList.add('btn-group');
        playPreviousGames.setAttribute('role', 'group');
        playPreviousGames.innerHTML = '<button id="btnGroupDrop1" type="button" class="btn btn-outline-success dropdown-toggle playButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">PLAY PREVIOUS MAPS</button>'
        let playPreviousGamesDropdown = document.createElement('div');
        playPreviousGamesDropdown.classList.add('dropdown-menu');
        playPreviousGamesDropdown.setAttribute('id', 'playPreviousGames');
        playPreviousGamesDropdown.setAttribute('aria-labelledby', 'btnGroupDrop1');
        playPreviousGames.append(playPreviousGamesDropdown);
    
        games.forEach(game => {
            let gameDomLink = document.createElement('a');
            let gameUrl = `https://www.geoguessr.com/challenge/${game.id}`;
    
            gameDomLink.setAttribute('href', gameUrl)
            gameDomLink.setAttribute('target', '_blank');
    
            if(game === games[games.length -1]) {
                gameDomLink.innerHTML = '<button type="button" class="btn btn-success playButton">PLAY LATEST GAME</button>';
                buttonsGroup.prepend(gameDomLink);
            } else {
                gameDomLink.classList.add('dropdown-item');
                gameDomLink.innerHTML = `${game.map} - ${game.date}`;
                playPreviousGamesDropdown.append(gameDomLink);
            }
        })

        if(games.length > 1) {
            buttonsGroup.append(playPreviousGames);
        }
    }
}