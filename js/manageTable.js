
function createTableRow(data, dom) {
    let row = document.createElement('tr');
    data.forEach(item => {
        row.innerHTML += item;
    });
    document.getElementById(dom).append(row);
}

function removeChilds(element) {
    var child = element.lastElementChild;  
    while (child) { 
        element.removeChild(child); 
        child = element.lastElementChild; 
    } 
}